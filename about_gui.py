# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'about_gui.ui'
##
## Created by: Qt User Interface Compiler version 6.6.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
                            QMetaObject, QObject, QPoint, QRect,
                            QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
                           QFont, QFontDatabase, QGradient, QIcon,
                           QImage, QKeySequence, QLinearGradient, QPainter,
                           QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QMainWindow, QSizePolicy, QTextBrowser,
                               QWidget)


class Ui_AboutWindow(object):
    def setupUi(self, AboutWindow):
        if not AboutWindow.objectName():
            AboutWindow.setObjectName(u"WelcomeWindow")
        AboutWindow.resize(600, 500)
        AboutWindow.setStyleSheet(
            u"background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:0, y2:0, stop:1 rgba(100,125,238,1), stop:0 rgba(127, 83, 172,1));\n"
            "font-family: Robot Mono Regular;\n"
            "            ")
        self.centralwidget = QWidget(AboutWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.textBrowser = QTextBrowser(self.centralwidget)
        self.textBrowser.setObjectName(u"textBrowser")
        self.textBrowser.setGeometry(QRect(20, 20, 560, 460))
        self.textBrowser.setStyleSheet(u"background-color: rgba(255, 255, 255, 30); \n"
                                       "border: 1px solid rgba(255,255,255,40);\n"
                                       "border-radius: 7px;\n"
                                       "color: rgb(255,255,255);\n"
                                       "font-weight: bold;\n"
                                       "font-size: 18pt;\n"
                                       "font: 10pt \"Roboto Mono\";\n"
                                       "")
        # AboutWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(AboutWindow)

        QMetaObject.connectSlotsByName(AboutWindow)

    # setupUi

    def retranslateUi(self, WelcomeWindow):
        WelcomeWindow.setWindowTitle(QCoreApplication.translate("WelcomeWindow", u"PyPass", None))
        self.textBrowser.setHtml(QCoreApplication.translate("WelcomeWindow",
                                                            u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                                            "<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
                                                            "p, li { white-space: pre-wrap; }\n"
                                                            "hr { height: 1px; border-width: 0; }\n"
                                                            "li.unchecked::marker { content: \"\\2610\"; }\n"
                                                            "li.checked::marker { content: \"\\2612\"; }\n"
                                                            "</style></head><body style=\" font-family:'Roboto Mono'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
                                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">PyPass - \u044d\u0442\u043e \u043f\u0440\u043e\u0433\u0440\u0430\u043c\u043c\u0430 \u0434\u043b\u044f \u043f\u043e\u043b\u0443\u0447\u0435\u043d\u0438\u044f \u0431\u0430\u0437\u043e\u0432\u044b\u0445 \u0437\u043d\u0430\u0439\u043d\u0438\u0439 \u043e \u044f\u0437\u044b\u043a\u0435 \u043f\u0440\u043e\u0433\u0440\u0430\u043c\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u044f"
                                                            " \u0432\u044b\u0441\u043a\u043e\u0433\u043e \u0443\u0440\u043e\u0432\u043d\u044f Python </span></p>\n"
                                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">\u0412 \u043f\u0440\u043e\u0433\u0440\u0430\u043c\u043c\u0435 \u043f\u0440\u0438\u0441\u0443\u0442\u0441\u0442\u0432\u0443\u0435\u0442 \u043b\u0438\u0448\u044c \u043e\u0441\u043d\u043e\u0432\u044b \u044f\u0437\u044b\u043a\u0430 (\u0440\u0430\u0431\u043e\u0442\u0430 \u0441 \u043f\u0435\u0440\u0435\u043c\u0435\u043d\u043d\u044b\u043c\u0438, \u0432\u0435\u0442\u0432\u043b\u0435\u043d\u0438\u044f\u043c\u0438, \u0446\u0438\u043a\u043b\u0430\u043c\u0438 \u0438 \u0441\u043f\u0438\u0441\u043a\u0430\u043c\u0438)</span></p>\n"
                                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;\"><br /></p></body></html>",
                                                            None))
    # retranslateUi
