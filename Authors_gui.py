# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'Authors_gui.ui'
##
## Created by: Qt User Interface Compiler version 6.6.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QMainWindow, QSizePolicy, QTextBrowser,
    QWidget)

class Ui_AuthorWindow(object):
    def setupUi(self, AuthorWindow):
        if not AuthorWindow.objectName():
            AuthorWindow.setObjectName(u"AuthorWindow")
        AuthorWindow.resize(400, 500)
        AuthorWindow.setStyleSheet(u"background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:0, y2:0, stop:1 rgba(100,125,238,1), stop:0 rgba(127, 83, 172,1));\n"
"font-family: Robot Mono Regular;\n"
"            ")
        self.centralwidget = QWidget(AuthorWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.textBrowser = QTextBrowser(self.centralwidget)
        self.textBrowser.setObjectName(u"textBrowser")
        self.textBrowser.setGeometry(QRect(20, 20, 360, 460))
        self.textBrowser.setStyleSheet(u"background-color: rgba(255, 255, 255, 30); \n"
"border: 1px solid rgba(255,255,255,40);\n"
"border-radius: 7px;\n"
"color: rgb(255,255,255);\n"
"font-weight: bold;\n"
"font-size: 18pt;\n"
"font: 10pt \"Roboto Mono\";\n"
"")
        ##AuthorWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(AuthorWindow)

        QMetaObject.connectSlotsByName(AuthorWindow)
    # setupUi

    def retranslateUi(self, AuthorWindow):
        AuthorWindow.setWindowTitle(QCoreApplication.translate("WelcomeWindow", u"PyPass", None))
        self.textBrowser.setHtml(QCoreApplication.translate("WelcomeWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"hr { height: 1px; border-width: 0; }\n"
"li.unchecked::marker { content: \"\\2610\"; }\n"
"li.checked::marker { content: \"\\2612\"; }\n"
"</style></head><body style=\" font-family:'Roboto Mono'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Robot Mono Regular'; font-size:18pt;\">\u0410\u0432\u0442\u043e\u0440\u044b:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Robot Mono Regular'; font-size:18pt;\">\u041b\u0438\u0442\u0432\u0438\u043d \u0410\u043d\u0442\u043e\u043d</span></p>\n"
"<p style="
                        "\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Robot Mono Regular'; font-size:18pt;\">\u0428\u043a\u043e\u0434\u0430 \u041f\u0430\u0432\u0435\u043b</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Robot Mono Regular'; font-size:18pt;\">\u041f\u043b\u044e\u0445\u0438\u043d \u0410\u043b\u0435\u043a\u0441\u0430\u043d\u0434\u0440</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Robot Mono Regular'; font-size:18pt;\">\u041f\u043b\u044e\u0445\u0438\u043d \u041a\u0438\u0440\u0438\u043b</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Robot Mono Regular'; font-size:18pt;\">\u041a\u0440\u0430"
                        "\u0435\u0432\u043e\u0439 \u0410\u0440\u0442\u0451\u043c</span></p></body></html>", None))
    # retranslateUi

