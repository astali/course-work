; ��� ����������
#define   Name       "PyPass"
; ������ ����������
#define   Version    "0.0.1"
; �����-�����������
#define   Publisher  "10-24"
; ���� ����� ������������
#define   URL        "https://gitlab.com/astali/course-work"
; ��� ������������ ������
#define   ExeName    "main.exe"

;------------------------------------------------------------------------------
;   ��������� ���������
;------------------------------------------------------------------------------
[Setup]

; ���������� ������������� ����������, 
;��������������� ����� Tools -> Generate GUID
AppId={{C31940DA-56FE-425F-ACBB-81B019AFB081}

; ������ ����������, ������������ ��� ���������
AppName={#Name}
AppVersion={#Version}
AppPublisher={#Publisher}
AppPublisherURL={#URL}
AppSupportURL={#URL}
AppUpdatesURL={#URL}
                               
; ���� ��������� ��-���������
DefaultDirName=C:\Users\{username}\AppData\Local\{#Name}
; ��� ������ � ���� "����"
DefaultGroupName={#Name}

; �������, ���� ����� ������� ��������� setup � ��� ������������ �����
OutputDir=C:\Users\anton\OneDrive\������� ����\Inno
OutputBaseFileName=PyPass

; ���� ������
SetupIconFile=D:\PycharmProjects\CourseWork\FilesForDist\PyPass.ico

; ��������� ������
Compression=lzma
SolidCompression=yes
;------------------------------------------------------------------------------
;   ������������� ����� ��� �������� ���������
;------------------------------------------------------------------------------
[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"; 
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"; 
;------------------------------------------------------------------------------
;   ����������� - ��������� ������, ������� ���� ��������� ��� ���������
;------------------------------------------------------------------------------
[Tasks]
; �������� ������ �� ������� �����
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked
;------------------------------------------------------------------------------
;   �����, ������� ���� �������� � ����� �����������
;------------------------------------------------------------------------------
[Files]

; ����������� ����
Source: "D:\PycharmProjects\CourseWork\dist\main.exe"; DestDir: "{app}"; Flags: ignoreversion

; ������������� �������
Source: "D:\PycharmProjects\CourseWork\PyPass\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
;------------------------------------------------------------------------------
;   ��������� �����������, ��� �� ������ ����� ������
;------------------------------------------------------------------------------ 
[Icons]

Name: "{group}\{#Name}"; Filename: "{app}\{#ExeName}"

Name: "{commondesktop}\{#Name}"; Filename: "{app}\{#ExeName}"; Tasks: desktopicon