# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'welcome_gui.ui'
##
## Created by: Qt User Interface Compiler version 6.6.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
                            QMetaObject, QObject, QPoint, QRect,
                            QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
                           QFont, QFontDatabase, QGradient, QIcon,
                           QImage, QKeySequence, QLinearGradient, QPainter,
                           QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QLabel, QMainWindow,
                               QPushButton, QSizePolicy, QWidget)


class Ui_WelcomeWindow(object):
    def setupUi(self, WelcomeWindow):
        if not WelcomeWindow.objectName():
            WelcomeWindow.setObjectName(u"WelcomeWindow")
        WelcomeWindow.resize(1163, 831)
        WelcomeWindow.setStyleSheet(
            u"background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:0, y2:0, stop:1 rgba(100,125,238,1), stop:0 rgba(127, 83, 172,1));\n"
            "font-family: Robot Mono Regular;\n"
            "            ")
        self.centralwidget = QWidget(WelcomeWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.frame = QFrame(self.centralwidget)
        self.frame.setObjectName(u"frame")
        self.frame.setGeometry(QRect(20, 20, 1121, 791))
        self.frame.setStyleSheet(u"background-color: rgba(255, 255, 255, 30); \n"
                                 "border: 1px solid rgba(255,255,255,40);\n"
                                 "border-radius: 7px;")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.Start = QPushButton(self.frame)
        self.Start.setObjectName(u"Start")
        self.Start.setGeometry(QRect(450, 430, 271, 71))
        self.Start.setStyleSheet(u"color: white;\n"
                                 "font-weight: bold;\n"
                                 "font-size: 20pt;\n"
                                 "background-color: rgb(255, 219, 88);\n"
                                 "border: 1px solid rgba(255,255,255,40);\n"
                                 "border-radius: 7px;")
        self.Authors = QPushButton(self.frame)
        self.Authors.setObjectName(u"Authors")
        self.Authors.setGeometry(QRect(450, 650, 271, 71))
        self.Authors.setStyleSheet(u"color: white;\n"
                                   "font-weight: bold;\n"
                                   "font-size: 20pt;\n"
                                   "background-color: rgb(255, 219, 88);\n"
                                   "border: 1px solid rgba(255,255,255,40);\n"
                                   "border-radius: 7px;")
        self.Title = QLabel(self.frame)
        self.Title.setObjectName(u"Title")
        self.Title.setGeometry(QRect(60, 80, 1031, 251))
        self.Title.setStyleSheet(u"font: 10pt \"Roboto Mono\";\n"
                                 "color: white;")
        self.Info = QPushButton(self.frame)
        self.Info.setObjectName(u"Info")
        self.Info.setGeometry(QRect(450, 540, 271, 71))
        self.Info.setStyleSheet(u"color: white;\n"
                                "font-weight: bold;\n"
                                "font-size: 20pt;\n"
                                "background-color: rgb(255, 219, 88);\n"
                                "border: 1px solid rgba(255,255,255,40);\n"
                                "border-radius: 7px;\n"
                                "")
        WelcomeWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(WelcomeWindow)

        QMetaObject.connectSlotsByName(WelcomeWindow)

    # setupUi

    def retranslateUi(self, WelcomeWindow):
        WelcomeWindow.setWindowTitle(QCoreApplication.translate("WelcomeWindow", u"PyPass", None))
        self.Start.setText(QCoreApplication.translate("WelcomeWindow", u"\u041d\u0430\u0447\u0430\u0442\u044c", None))
        self.Authors.setText(
            QCoreApplication.translate("WelcomeWindow", u"\u041e\u0431 \u0430\u0432\u0442\u043e\u0440\u0430\u0445",
                                       None))
        self.Title.setText(QCoreApplication.translate("WelcomeWindow",
                                                      u"<html><head/><body><p align=\"center\"><span style=\" font-size:48pt;\">\u0414\u043e\u0431\u0440\u043e \u043f\u043e\u0436\u0430\u043b\u043e\u0432\u0430\u0442\u044c \u0432 PyPass</span></p><p align=\"center\"><br/></p></body></html>",
                                                      None))
        self.Info.setText(QCoreApplication.translate("WelcomeWindow", u"O PyPass", None))
    # retranslateUi
