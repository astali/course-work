import os
import sys





def save(self):
    '''Метод сохраняющий введённый код '''
    print("Saved")
    Text = self.CodeEditor.toPlainText()

    save_path = os.getenv("LOCALAPPDATA")
    name_of_dir1 = "PyPass"
    name_of_dir2 = "Tests"
    name_of_file = "Testo"
    completeName = os.path.join(save_path, name_of_dir1, name_of_dir2, name_of_file + ".py")
    file1 = open(completeName, "w")

    file1.write(Text)

    file1.close()


def saveForTeacher(self):
    '''Метод сохраняющий введённый код для проверки преподователем'''
    Text = self.CodeEditor.toPlainText()

    save_path = os.getenv("LOCALAPPDATA")
    name_of_dir1 = "PyPass"
    name_of_dir2 = "TeacherToCheck"
    name_of_file = ["forTeacher1", "forTeacher2", "forTeacher3", "forTeacher4", "forTeacher5", "forTeacher6","forTeacher7","forTeacher8"]
    completeName = os.path.join(save_path, name_of_dir1, name_of_dir2, name_of_file[self.numberofTask] + ".txt")
    fileT = open(completeName, "w")

    fileT.write(Text)

    fileT.close()


def run(self):
    '''Метод запускающий код'''
    print("Running")

    save_path = os.getenv("LOCALAPPDATA")
    name_of_dir1 = "PyPass"
    name_of_dir2 = "Tests"
    name_of_file = "Testo"
    completeName = os.path.join(save_path, name_of_dir1, name_of_dir2, name_of_file + ".py")

    os.system("py " + completeName)

    # setupUi


def taskget(self):
    '''Метод который меняет задание на следующее'''
    if Answer(self) == True:
        save_path = os.getenv("LOCALAPPDATA")
        name_of_dir1 = "PyPass"
        name_of_dir2 = "Tasks"
        name_of_file = ["Task1", "Task2", "Task3", "Task4", "Task5", "Task6","Task7","Task8"]

        completeName2 = os.path.join(save_path, name_of_dir1, name_of_dir2, name_of_file[self.numberofTask] + ".txt")
        file2 = open(completeName2, "r", encoding='utf-8')
        text = file2.read()
        self.TaskSpace.setText(text)
        self.numberofTask += 1


def theoryget(self):
    '''Метод меняющий теоретическую информаию '''
    save_path = os.getenv("LOCALAPPDATA")

    name_of_dir3 = "Theory"
    name_of_file = ["Theory1", "Theory2", "Theory3", "Theory4", "Theory5"]

    completeName2 = os.path.join(save_path, "Pypass", "Theory", name_of_file[self.numberofTask] + ".txt")
    file2 = open(completeName2, "r", encoding='utf-8')
    text = file2.read()
    self.TheoryText.setText(text)


def Answer(self):
    '''Метод проверяющий введёный ответ и возвращающий True если ответ верный'''
    Ans = self.AnswerSpace.toPlainText()
    save_path = os.getenv("LOCALAPPDATA")

    name_of_file = ["Answer1", "Answer2", "Answer3","Answer4","Answer5","Answer6","Answer7","Answer8"]
    completeName2 = os.path.join(save_path, "Pypass", "Answers", name_of_file[self.numberofTask] + ".txt")
    file = open(completeName2, "r", encoding='utf-8')
    text = file.read()
    if text == Ans:
        return True
    else:
        return False
