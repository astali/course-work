# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'gui_pypass.ui'
##
## Created by: Qt User Interface Compiler version 6.6.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################
import os.path
import os
from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
                            QMetaObject, QObject, QPoint, QRect,
                            QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
                           QFont, QFontDatabase, QGradient, QIcon,
                           QImage, QKeySequence, QLinearGradient, QPainter,
                           QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QLabel, QMainWindow,
                               QPlainTextEdit, QPushButton, QSizePolicy, QTextBrowser,
                               QWidget, QFileDialog)
from PySide6 import QtWidgets
from theory import Ui_TheoryWindow

class Ui_TaskWindow(object):
    from methods_pypass import save,run,taskget,theoryget,saveForTeacher
    '''Класс описывающий информацию об окне с заданиями'''
    def __init__(self):
        self.numberofTask=0
    def setupUi(self, MainWindow):

        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1163, 831)
        MainWindow.setStyleSheet(
            u"background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:0, y2:0, stop:1 rgba(100,125,238,1), stop:0 rgba(127, 83, 172,1));\n"
            "font-family: Robot Mono Regular;\n"
            "")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.frame = QFrame(self.centralwidget)
        self.frame.setObjectName(u"frame")
        self.frame.setGeometry(QRect(20, 20, 1121, 791))
        self.frame.setStyleSheet(u"background-color: rgba(255, 255, 255, 30); \n"
                                 "border: 1px solid rgba(255,255,255,40);\n"
                                 "border-radius: 7px;")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)

        font = QFont()
        font.setFamilies([u"Robot Mono Regular"])
        font.setPointSize(20)
        font.setBold(True)
        font.setStrikeOut(False)
        font.setKerning(True)

        self.CodeEditor = QPlainTextEdit(self.frame)
        self.CodeEditor.setObjectName(u"CodeEditor")
        self.CodeEditor.setGeometry(QRect(588, 80, 521, 470))
        self.CodeEditor.setStyleSheet(u"color: white;\n"
                                      "font-weight: bold;\n"
                                      "font-size: 20pt;\n"
                                      "background-color: rgba( 255,255,255,30);\n"
                                      "border: 1px solid rgba( 255,255,255,30);\n"
                                      "border-radius: 9px;")

        self.AnswerSpace = QPlainTextEdit(self.frame)
        self.AnswerSpace.setObjectName(u"AnswerSpace")
        self.AnswerSpace.setGeometry(QRect(588, 570, 521, 120))
        self.AnswerSpace.setStyleSheet(u"color: white;\n"
                                      "font-weight: bold;\n"
                                      "font-size: 20pt;\n"
                                      "background-color: rgba( 255,255,255,30);\n"
                                      "border: 1px solid rgba( 255,255,255,30);\n"
                                      "border-radius: 9px;")

        self.TaskSpace = QTextBrowser(self.frame)
        self.TaskSpace.setObjectName(u"TaskSpace")
        self.TaskSpace.setGeometry(QRect(10, 80, 551, 610))
        self.TaskSpace.setStyleSheet(u"color: white;\n"
                                     "font-weight: bold;\n"
                                     "font-size: 20pt;\n"
                                     "background-color: rgba( 255,255,255,30);\n"
                                     "border: 1px solid rgba( 255,255,255,30);\n"
                                     "border-radius: 9px;\n"
                                     "")

        self.NameTask = QLabel(self.frame)
        self.NameTask.setObjectName(u"NameTask")
        self.NameTask.setGeometry(QRect(10, 20, 551, 50))
        font1 = QFont()
        font1.setFamilies([u"Robot Mono Regular"])
        font1.setPointSize(18)
        font1.setBold(True)
        self.NameTask.setFont(font1)
        self.NameTask.setStyleSheet(u"color: white;\n"
                                    "font-weight: bold;\n"
                                    "font-size: 18pt;\n"
                                    "background-color: rgba( 255,255,255,30);\n"
                                    "border: 1px solid rgba( 255,255,255,30);\n"
                                    "border-radius: 9px;\n"
                                    "\n"
                                    "")
        self.CodeSpace = QLabel(self.frame)
        self.CodeSpace.setObjectName(u"CodeSpace")
        self.CodeSpace.setGeometry(QRect(588, 20, 521, 50))
        self.CodeSpace.setStyleSheet(u"color: white;\n"
                                     "font-weight: bold;\n"
                                     "font-size: 18pt;\n"
                                     "background-color: rgba( 255,255,255,30);\n"
                                     "border: 1px solid rgba( 255,255,255,30);\n"
                                     "border-radius: 9px;\n"
                                     "")
        self.SaveCode = QPushButton(self.frame)
        self.SaveCode.setObjectName(u"SaveCode")
        self.SaveCode.setGeometry(QRect(588, 720, 161, 61))
        self.SaveCode.setStyleSheet(u"color: white;\n"
                                    "font-weight: bold;\n"
                                    "font-size: 20pt;\n"
                                    "background-color: rgb(255, 219, 88);\n"
                                    "border: 1px solid rgba(255,255,255,40);\n"
                                    "border-radius: 7px;")
        self.RunCode = QPushButton(self.frame)
        self.RunCode.setObjectName(u"RunCode")
        self.RunCode.setGeometry(QRect(768, 720, 161, 61))
        self.RunCode.setFont(font)
        self.RunCode.setStyleSheet(u"color: white;\n"
                                   "font-weight: bold;\n"
                                   "font-size: 20pt;\n"
                                   "background-color: rgb(255, 219, 88);\n"
                                   "border: 1px solid rgba(255,255,255,40);\n"
                                   "border-radius: 7px;")
        self.Submit = QPushButton(self.frame)
        self.Submit.setObjectName(u"Submit")
        self.Submit.setGeometry(QRect(948, 720, 161, 61))
        self.Submit.setStyleSheet(u"color: white;\n"
                                  "font-weight: bold;\n"
                                  "font-size: 20pt;\n"
                                  "background-color: rgb(255, 219, 88);\n"
                                  "border: 1px solid rgba(255,255,255,40);\n"
                                  "border-radius: 7px;")
        self.Theory = QPushButton(self.frame)
        self.Theory.setObjectName(u"Theory")
        self.Theory.setGeometry(QRect(10, 720, 161, 61))
        self.Theory.setStyleSheet(u"color: white;\n"
                                  "font-weight: bold;\n"
                                  "font-size: 20pt;\n"
                                  "background-color: rgb(255, 219, 88);\n"
                                  "border: 1px solid rgba(255,255,255,40);\n"
                                  "border-radius: 7px;")


        ##MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        self.functions_ui(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)

    def functions_ui(self, MainWindow):
        self.SaveCode.clicked.connect(self.save)
        self.RunCode.clicked.connect(self.run)
        self.Submit.clicked.connect(self.taskget)
        self.Theory.clicked.connect(self.openTheory)
        self.Submit.clicked.connect(self.saveForTeacher)
        ##self.Theory.clicked.connect(self.theoryget)
    def openTheory(self,MainWindow):
        '''Функция октрывающая окно с теоретической информацией'''
        self.new_window = QtWidgets.QDialog()
        self.ui_window = Ui_TheoryWindow(self.numberofTask)
        self.ui_window.setupUi(self.new_window)
        self.new_window.show()
    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"PyPass", None))
        self.RunCode.setText(QCoreApplication.translate("MainWindow", u"Run Code", None))
        self.Submit.setText(QCoreApplication.translate("MainWindow", u"Submit", None))
        '''
        self.TaskSpace.setHtml(QCoreApplication.translate("MainWindow",
                                                          u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                                          "<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
                                                          "p, li { white-space: pre-wrap; }\n"
                                                          "hr { height: 1px; border-width: 0; }\n"
                                                          "li.unchecked::marker { content: \"\\2610\"; }\n"
                                                          "li.checked::marker { content: \"\\2612\"; }\n"
                                                          "</style></head><body style=\" font-family:'Robot Mono Regular'; font-size:20pt; font-weight:700; font-style:normal;\">\n"
                                                          "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\u0417\u043d\u0430\u0447\u0438\u0442, \u0442\u0432\u043e\u044f \u0437\u0430\u0434\u0430\u0447\u0430 \u043d\u0430\u043f\u0438\u0441\u0430\u0442\u044c    Hello world \u043d\u0430 Python</p></body></html>",
                                                          None))
        '''

        self.TaskSpace.setHtml(QCoreApplication.translate("MainWindow",u"Нужно написать функцию task() которая возвращает 'Hello World' на Python \n\n(функция должна называться task()  )",None))
        self.NameTask.setText(QCoreApplication.translate("MainWindow", u"Your task", None))
        self.CodeSpace.setText(QCoreApplication.translate("MainWindow", u"Space to type your code", None))
        self.SaveCode.setText(QCoreApplication.translate("MainWindow", u"Save Code", None))
        self.Theory.setText(QCoreApplication.translate("MainWindow",u"Theory",None))
    # retranslateUi
