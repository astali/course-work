import sys
import os
from PySide6.QtWidgets import QApplication, QMainWindow
from PySide6 import QtWidgets

from gui_pypass import Ui_TaskWindow
from welcome_gui import Ui_WelcomeWindow
from Authors_gui import Ui_AuthorWindow
from about_gui import Ui_AboutWindow


class Welcome(QMainWindow):
    '''Основной класс, в который содержит вызов всех окон и подокон приложения'''
    def __init__(self):
        super(Welcome, self).__init__()
        self.ui = Ui_WelcomeWindow()
        self.ui.setupUi(self)

        self.ui.Start.clicked.connect(self.open_task_widow)
        self.ui.Authors.clicked.connect(self.open_author_widow)
        self.ui.Info.clicked.connect(self.open_about_window)

    def open_task_widow(self):
        '''метод открывающий окно с заданиями'''
        self.new_window = QtWidgets.QDialog()
        self.ui_window = Ui_TaskWindow()
        self.ui_window.setupUi(self.new_window)
        self.new_window.show()

    def open_author_widow(self):
        '''метод открывающий окно с информацией про авторов'''
        self.new_window2 = QtWidgets.QDialog()
        self.ui_window2 = Ui_AuthorWindow()
        self.ui_window2.setupUi(self.new_window2)
        self.new_window2.show()

    def open_about_window(self):
        '''метод открывающий окно с информацией о приложениии '''
        self.new_window3 = QtWidgets.QDialog()
        self.ui_window3 = Ui_AboutWindow()
        self.ui_window3.setupUi(self.new_window3)
        self.new_window3.show()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Welcome()
    window.show()

    sys.exit(app.exec())
