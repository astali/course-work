# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'theory.ui'
##
## Created by: Qt User Interface Compiler version 6.6.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
                            QMetaObject, QObject, QPoint, QRect,
                            QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
                           QFont, QFontDatabase, QGradient, QIcon,
                           QImage, QKeySequence, QLinearGradient, QPainter,
                           QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QMainWindow, QSizePolicy,
                               QTextBrowser, QWidget)

class Ui_TheoryWindow(object):
    from methods_pypass import theoryget
    def __init__(self,numberofTask=None):
        self.numberofTask=numberofTask
    def setupUi(self, TheoryWindow):
        if not TheoryWindow.objectName():
            TheoryWindow.setObjectName(u"TheoryWindow")
        TheoryWindow.resize(1040, 640)
        TheoryWindow.setStyleSheet(
            u"background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:0, y2:0, stop:1 rgba(100,125,238,1), stop:0 rgba(127, 83, 172,1));\n"
            "font-family: Robot Mono Regular;\n"
            "            ")
        self.centralwidget = QWidget(TheoryWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.frame = QFrame(self.centralwidget)
        self.frame.setObjectName(u"frame")
        self.frame.setGeometry(QRect(20, 20, 1000, 600))
        self.frame.setStyleSheet(u"background-color: rgba(255, 255, 255, 30); \n"
                                 "border: 1px solid rgba(255,255,255,40);\n"
                                 "border-radius: 7px;")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.TheoryText = QTextBrowser(self.frame)
        self.TheoryText.setObjectName(u"TheoryText")
        self.TheoryText.setGeometry(QRect(0, 0, 1000, 600))
        self.TheoryText.setStyleSheet(u"background-color: rgba(255, 255, 255, 0); \n"
                                      "border: 1px solid rgba(255,255,255,0);\n"
                                      "border-radius: 7px;\n"
                                      u"color: white;\n"
                                      "font-weight: bold;\n"
                                      "font-size: 18pt;\n"
                                      )
        ##TheoryWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(TheoryWindow)
        self.theoryget()
        QMetaObject.connectSlotsByName(TheoryWindow)

    # setupUi

    def retranslateUi(self, TheoryWindow):
        TheoryWindow.setWindowTitle(QCoreApplication.translate("TheoryWindow", u"PyPass", None))
    # retranslateUi
